/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)
  Copyright 2010  Matthias Hullin (lufa [at] matthias [dot] hullin [dot] net)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Main source file for the MassStorageKeyboard demo. This file contains the main tasks of
 *  the demo and is responsible for the initial application hardware configuration.
 */

#include "MassStorageGenericHID.h"
#include "Lib/24cXXX.h"
#include "debug.h"
#include "Settings.h"
#include "dataOutputVars.h"

#if MASS_STORAGE_ENABLED == 1
/** LUFA Mass Storage Class driver interface configuration and state information. This structure is
 *  passed to all Mass Storage Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_MS_Device_t Disk_MS_Interface =
	{
		.Config =
			{
				.InterfaceNumber           = INTERFACE_ID_MassStorage,
				.DataINEndpoint            =
					{
						.Address           = MASS_STORAGE_IN_EPADDR,
						.Size              = MASS_STORAGE_IO_EPSIZE,
						.Banks             = 1,
					},
				.DataOUTEndpoint            =
					{
						.Address           = MASS_STORAGE_OUT_EPADDR,
						.Size              = MASS_STORAGE_IO_EPSIZE,
						.Banks             = 1,
					},
				.TotalLUNs                 = TOTAL_LUNS,
			},
	};
#endif

/** Buffer to hold the previously generated HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevHIDReportBuffer[sizeof(struct FeatureReportParameters) > GENERIC_REPORT_SIZE ?
										sizeof(struct FeatureReportParameters) : 
										GENERIC_REPORT_SIZE
								];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Generic_HID_Interface =
	{
		.Config =
			{
				.InterfaceNumber              = INTERFACE_ID_GenericHID,
				.ReportINEndpoint             =
					{
						.Address              = GENERIC_IN_EPADDR,
						.Size                 = GENERIC_EPSIZE,
						.Banks                = 1,
					},
				.PrevReportINBuffer           = PrevHIDReportBuffer,
				.PrevReportINBufferSize       = sizeof(PrevHIDReportBuffer),
			},
	};
	
static struct _pointer mempointer = {
	.PointerType = POINTER_TYPE_SETTINGS,
	.Offset = 0,
};

/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
/************************************************************************/
/*  DISABLED                                                            */
/************************************************************************
int main(void)
{
	SetupHardware();

	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	GlobalInterruptEnable();

	for (;;)
	{
		MS_Device_USBTask(&Disk_MS_Interface);
		HID_Device_USBTask(&Keyboard_HID_Interface);
		USB_USBTask();
	}
} //*/

void LUFA_main()
{
	HID_Device_USBTask(&Generic_HID_Interface);
#if MASS_STORAGE_ENABLED == 1
	MS_Device_USBTask(&Disk_MS_Interface);
#endif
	USB_USBTask();
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
#if (ARCH == ARCH_AVR8)
	/* Disable watchdog if enabled by bootloader/fuses */
#if USE_WDT != 1
	MCUSR &= ~(1 << WDRF);
	wdt_disable();
#endif
	/* Disable clock division */
	clock_prescale_set(clock_div_1);
#elif (ARCH == ARCH_XMEGA)
	/* Start the PLL to multiply the 2MHz RC oscillator to 32MHz and switch the CPU core to run from it */
	XMEGACLK_StartPLL(CLOCK_SRC_INT_RC2MHZ, 2000000, F_CPU);
	XMEGACLK_SetCPUClockSource(CLOCK_SRC_PLL);

	/* Start the 32MHz internal RC oscillator and start the DFLL to increase it to 48MHz using the USB SOF as a reference */
	XMEGACLK_StartInternalOscillator(CLOCK_SRC_INT_RC32MHZ);
	XMEGACLK_StartDFLL(CLOCK_SRC_INT_RC32MHZ, DFLL_REF_INT_USBSOF, F_USB);

	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
#endif

	/* Hardware Initialization */

	LEDs_Init();
	Dataflash_Init();
	//USB_Init();
	
	/* Check if the Dataflash is working, abort if not */
	if (!(DataflashManager_CheckDataflashOperation()))
	{
#if USB_CONTROLS_LEDS == 1
		LEDs_SetAllLEDs(LEDMASK_USB_ERROR);
#endif
		for(;;);
	}

	/* Clear Dataflash sector protections, if enabled */
	DataflashManager_ResetDataflashProtections();
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
#if USB_CONTROLS_LEDS == 1
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
#endif
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
#if USB_CONTROLS_LEDS == 1
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
#endif
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Generic_HID_Interface);
#if MASS_STORAGE_ENABLED == 1
	ConfigSuccess &= MS_Device_ConfigureEndpoints(&Disk_MS_Interface);
#endif
	USB_Device_EnableSOFEvents();
	
#if USB_CONTROLS_LEDS == 1
	LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
#endif
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
#if MASS_STORAGE_ENABLED == 1
	MS_Device_ProcessControlRequest(&Disk_MS_Interface);
#endif
	HID_Device_ProcessControlRequest(&Generic_HID_Interface);
}

#if MASS_STORAGE_ENABLED == 1
/** Mass Storage class driver callback function the reception of SCSI commands from the host, which must be processed.
 *
 *  \param[in] MSInterfaceInfo  Pointer to the Mass Storage class interface configuration structure being referenced
 */
bool CALLBACK_MS_Device_SCSICommandReceived(USB_ClassInfo_MS_Device_t* const MSInterfaceInfo)
{
	bool CommandSuccess;
	CommandSuccess = SCSI_DecodeSCSICommand(MSInterfaceInfo);
	return CommandSuccess;
}
#endif

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
    HID_Device_MillisecondElapsed(&Generic_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */

#if !MESURMENT_ENABLE
int counter = 0;
#endif

bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
	// ����� ��������� ������
	switch(ReportType)
	{
	case HID_REPORT_ITEM_In:
#if (!MESURMENT_ENABLE)
		{
			uint8_t s1 = counter % 10;
			uint8_t s2 = counter / 10;
				
			dataOutputVars.CurrentTemperature = s2 * 2 - s1;
			dataOutputVars.TargetTemperature = 750 - s1 * 3 + s2;
			++counter;
		}
#endif
		// ������� ����� � �������
		memcpy(ReportData, &dataOutputVars, GENERIC_REPORT_SIZE);
		*ReportSize = GENERIC_REPORT_SIZE;
		break;
			
	case HID_REPORT_ITEM_Feature:
		{
			struct FeatureReportParameters* result = ReportData;
			switch(mempointer.PointerType)
			{
			case POINTER_TYPE_SETTINGS:
				{
					mempointer.Offset %= sizeof(struct sSettings);
					uint8_t _size = sizeof(struct sSettings) - mempointer.Offset;
					if (_size > RW_BLOCK_SIZE)
					_size = RW_BLOCK_SIZE;
					eeprom_read_block(result->d, (uint8_t*)&settings + mempointer.Offset, _size);
					mempointer.Offset += _size;
					result->size = _size;
				}
				break;
			case POINTER_TYPE_DATA:
				EEReadBytes(result->d, mempointer.Offset, RW_BLOCK_SIZE);
				mempointer.Offset += RW_BLOCK_SIZE;
				result->size = RW_BLOCK_SIZE;
				break;
			case POINTER_DATE:
				{
					mempointer.Offset %= sizeof(struct ds1307DateTime);
					uint8_t _size = sizeof(struct ds1307DateTime) - mempointer.Offset;
					if (_size > RW_BLOCK_SIZE)
						_size = RW_BLOCK_SIZE;
					memcpy(result->d, (uint8_t*)&dataOutputVars.dateTime + mempointer.Offset, _size);
					mempointer.Offset += _size;
					result->size = _size;
				}
				break;
			default:
				memset(result->d, 0, RW_BLOCK_SIZE);
				result->size = 0;
				break;
			}

			*ReportSize = sizeof(struct FeatureReportParameters);
		}
		break;
	default:
		*ReportSize = 0;
		break;
	}
	return false;
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
	// ����� ��������� �����-���� ������� ��� �������� ������
			
	switch(ReportType)
	{
	case HID_REPORT_ITEM_Out:
		if (ReportSize == sizeof(struct _pointer))
			memcpy(&mempointer, ReportData, sizeof(struct _pointer));		
		break;
	case HID_REPORT_ITEM_Feature:
	{
		struct FeatureReportParameters* data = ReportData;
		switch (mempointer.PointerType)
		{
		case POINTER_TYPE_SETTINGS:
			eeprom_write_block(data->d, (uint8_t*)&settings + mempointer.Offset, data->size);
			mempointer.Offset = (mempointer.Offset + data->size) % sizeof(struct sSettings);
			break;
		case POINTER_TYPE_DATA:
			EEWriteBytes(mempointer.Offset, data->d, data->size);
			mempointer.Offset += data->size;
			break;
		case POINTER_DATE:
		{
			struct ds1307DateTime date_buff;
			memcpy(&date_buff, &dataOutputVars.dateTime, sizeof(struct ds1307DateTime)); // curent date
			uint8_t writepos = mempointer.Offset % sizeof(struct ds1307DateTime);
			uint8_t writesize = data->size;
			if (writepos + writesize > sizeof(struct ds1307DateTime))
				writesize = sizeof(struct ds1307DateTime) - writepos;
			memcpy((uint8_t*)&date_buff + writepos, data->d, writesize);
			ds1307_setdate(&date_buff);
		}
			break;
		default:
			break;
		}		
	}
	break;	
	}
}

