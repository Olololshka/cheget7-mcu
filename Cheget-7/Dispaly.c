/*
 * Dispaly.c
 *
 * Created: 14.07.2015 10:24:36
 *  Author: tolyan
 */ 

#include <avr/wdt.h>

#include "Config/AppConfig.h"

#include "PCF8574.h"

#include "delay_wraper.h"
#include "Shaduler.h"

#include "Display.h"

//============================== DEFINES ==============

#define En 0b00000100  // Enable bit
#define Rw 0b00000010  // Read/Write bit
#define Rs 0b00000001  // Register select bit

#define command(val)	send(val, 0)
#define write(val)		send(val, Rs)

//============================== /DEFINES ==============

static void pulseEnable(uint8_t _data){
	Extender_Write(_data | En);	// En high
	delay_us(1);		// enable pulse must be >450ns
	
	Extender_Write(_data & ~En);	// En low
	//delay_us(50);		// commands need > 37us to settle
	Shaduler_wait(40);
}

static void write4bits(uint8_t value) 
{
	Extender_Write(value);
	delay_us(2);
	pulseEnable(value);
}

// write either command or data
static void send(uint8_t value, uint8_t mode) {
	uint8_t highnib=value&0xf0;
	uint8_t lownib=(value<<4)&0xf0;
	write4bits((highnib)|mode);
	write4bits((lownib)|mode);
	
	shaduler_ProcessTimeCrytical();
}

static uint8_t putcount;

static void _putc(char data)
{
	++putcount;
	write(data);
}

void Display_constructor(struct sDisplayCtrl* this, uint8_t lcd_Addr,uint8_t lcd_cols, uint8_t lcd_rows, uint8_t backlight)
{
	this->_Addr = lcd_Addr;
	this->_cols = lcd_cols;
	this->_rows = lcd_rows;
	this->_backlightval = backlight;
	this->_displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
}

void Display_init(struct sDisplayCtrl* this, uint8_t dotsize)
{
	Extender_Init(this->_Addr | PCF8574_ADR_BASE);
	
	if (this->_rows > 1) {
		this->_displayfunction |= LCD_2LINE;
	}
	this->_numlines = this->_rows;
/*	
	for (uint8_t i = 0; i < 10; ++i)
	{
#if USE_WDT == 1
		wdt_reset();
		delay_us(50000);
#endif
	}
*/
	
	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
	delay_us(50000);

	// for some 1 line displays you can select a 10 pixel high font
	if ((dotsize != LCD_5x8DOTS) && (this->_numlines == 1)) {
		this->_displayfunction |= LCD_5x10DOTS;
	}
	
#if USE_WDT == 1
	wdt_reset();
#endif
	
	// Now we pull both RS and R/W low to begin commands
	Extender_setAllwaysSet(this->_backlightval);	// reset expanderand turn backlight off (Bit 8 =1)
	delay_us(1000);
	

	write4bits(0);
	delay_us(45); // wait min 39us
	
	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46
	
	// we start in 8bit mode, try to set 4 bit mode
	write4bits(0x03);
	delay_us(4500); // wait min 4.1ms
	
	// second try
	write4bits(0x03);
	delay_us(4500); // wait min 4.1ms
	
	// third go!
	write4bits(0x03);
	delay_us(150);

#ifdef HD4770	
	// finally, set to 4-bit interface
	write4bits(0x02);
	delay_us(150);
#endif

	// set # lines, font size, etc.
	command(LCD_FUNCTIONSET | this->_displayfunction);
	
#ifndef HD4770
	delay_us(40);
	command(LCD_FUNCTIONSET | this->_displayfunction);
#endif
	
	// turn the display on with no cursor or blinking default
	this->_displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	Display_displayOn(this);
		
	// clear it off
	Display_Clear(this);
	
	// Initialize to default text direction (for roman languages)
	this->_displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	
	// set the entry mode
	command(LCD_ENTRYMODESET | this->_displaymode);
	
	Display_home(this);

	fdev_setup_stream(&this->displayout, _putc, NULL, _FDEV_SETUP_WRITE);
	
#if USE_WDT == 1
	wdt_reset();
#endif
}

void Display_displayOn(struct sDisplayCtrl* this)
{
	this->_displaycontrol |= LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | this->_displaycontrol);
}

void Display_Clear(struct sDisplayCtrl* this)
{
	command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	delay_us(2000);  // this command takes a long time!
}

void Display_home(struct sDisplayCtrl* this)
{
	command(LCD_RETURNHOME);  // set cursor position to zero
	delay_us(2000);  // this command takes a long time!
}

void Display_SetCursor(struct sDisplayCtrl* this, uint8_t column, uint8_t row)
{
	const uint8_t row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > this->_numlines ) 
		row = this->_numlines - 1;    // we count rows starting w/0
	
	command(LCD_SETDDRAMADDR | (column + row_offsets[row]));
	this->xpos = column;
}

uint8_t Display_currentX(struct sDisplayCtrl* this)
{
	return this->xpos;
}

int Display_printf(struct sDisplayCtrl* this, char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	putcount = 0;
	vfprintf(&this->displayout, fmt, ap);
	this->xpos += putcount;
	return putcount;
}

int Display_printf_P(struct sDisplayCtrl* this, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	putcount = 0;
	vfprintf_P(&this->displayout, fmt, ap);
	this->xpos += putcount;
	return putcount;
}

void Display_putchar(struct sDisplayCtrl* this, uint8_t c)
{
	fputc(c, &this->displayout);
	++this->xpos;
}

void Display_finishLine(struct sDisplayCtrl* this)
{
	for (uint8_t i = Display_currentX(this); i < 16; ++i)
		Display_putchar(this, ' ');
}