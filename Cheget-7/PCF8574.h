/*
 * PCF8574.h
 *
 * Created: 13.07.2015 16:37:00
 *  Author: tolyan
 */ 


#ifndef PCF8574_H_
#define PCF8574_H_

#define PCF8574_ADR_BASE		(0b01000000)

void Extender_Init(uint8_t addr);
void Extender_Write(uint8_t value);
void Extender_setAllwaysSet(uint8_t mask);
void Extender_setAllwaysClear(uint8_t mask);

#endif /* PCF8574_H_ */