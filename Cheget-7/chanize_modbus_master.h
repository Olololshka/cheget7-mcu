/*
 * chanize_modbus_master.h
 *
 * Created: 04.12.2014 13:14:05
 *  Author: tolyan
 */ 


#ifndef CHANIZE_MODBUS_MASTER_H_
#define CHANIZE_MODBUS_MASTER_H_


int rtu_data_anlys(int *dest_p,unsigned char *source_p,int data_start_address,int fr_lenth);
int rtu_read_hldreg(unsigned char board_adr,unsigned char *com_buf,int start_address,int lenth);
int rtu_set_hldreg( unsigned char board_adr,unsigned char *com_buf,int start_address,int value );

#endif /* CHANIZE_MODBUS_MASTER_H_ */