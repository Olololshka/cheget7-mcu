/*
 * UI_Advanced.c
 *
 * Created: 27.08.2015 9:19:37
 *  Author: tolyan
 */ 

#include <avr/pgmspace.h>
#include <string.h>
#include <math.h>

#include "Display.h"
#include "Termoprogramm.h"
#include "dataOutputVars.h"
#include "ButtonsProcessor.h"

#include "UI.h"

enum enEditStep {
	ES_IDLE = 0,
	ES_STEP,
	ES_UST,
	ES_TIME,
	
	ES_EXEC_DISPLAY,
};

static struct {
	enum TermProgStep visableStep;
	enum enEditStep editStep;
} advModedata;

static const char Gotov[] PROGMEM = {' ', ' ', ' ', ' ', ' ', 0b10100001, 'o', 0b10111111, 'o', 0b10110011, 0};
static const char helpStr[] PROGMEM = {'O', 'k', '-', 'C', 0b10111111, 'a', 'p', 0b10111111, ' ',
	UpArrow, '/', DownArrow, '-', 0b10101000, 'p', '.',
	0};

static struct StepData tpSteps[T_PRG_STEP_END + 1];

static void UpdateTermoprogrammExecStatues(enum TermProgStep step);
static void sel_btn_callback(void);
static void removeCurrentStep(void);
static void UI_init(void);

static struct sTermoprogramm prog = {
	.steps = &tpSteps[1],
	.len = T_PRG_STEP_END,
	.callback_stepSwitched = UpdateTermoprogrammExecStatues
};

static void LoadTermoprogramm(struct StepData *place)
{
	for (enum TermProgStep i = T_PRG_STEP_1; i < T_PRG_STEPS; ++i)
		place[i] = Tp_getStep(i);
}

static void SaveTermoProgramm(struct StepData *prg)
{
	static struct StepData old[T_PRG_STEP_END + 1];
	LoadTermoprogramm(old);
	if (memcmp(&old[1], &tpSteps[1], T_PRG_STEP_END * sizeof(struct StepData)))
		for (enum TermProgStep i = T_PRG_STEP_1; i < T_PRG_STEPS; ++i)
			Tp_setStep(i, &tpSteps[i]);
}

static void next_prg_step(void)
{
	++advModedata.visableStep;
	if (advModedata.visableStep > T_PRG_STEP_END)
		advModedata.visableStep = T_PRG_STEP_END;
	else
	{
		if (advModedata.visableStep > T_PRG_STEP_1 && tpSteps[advModedata.visableStep - 1].stepID == T_PRG_STEP_IDLE)
			--advModedata.visableStep;
	}

	advModedata.editStep = ES_STEP;
	btns[BTN_SEL].onLongPush = removeCurrentStep;
}

static void prev_prg_step(void)
{
	if (advModedata.visableStep > T_PRG_STEP_IDLE)
	{		
		if (advModedata.visableStep != T_PRG_STEP_1)
			--advModedata.visableStep;
	}
	else
		advModedata.visableStep = T_PRG_STEP_1;

	advModedata.editStep = ES_STEP;
	btns[BTN_SEL].onLongPush = removeCurrentStep;
}

static void removeCurrentStep(void)
{
	struct StepData empty = { .stepID = T_PRG_STEP_IDLE };
	tpSteps[advModedata.visableStep] = empty;
	constructTermoprogramm(&prog, dataOutputVars.CurrentTemperature);
	prev_prg_step();
}

static void reconstruct_tprog(void)
{
	tpSteps[advModedata.visableStep].stepID = advModedata.visableStep;
	constructTermoprogramm(&prog, dataOutputVars.CurrentTemperature);
}

//--------------------------------------------------------------------------

static void ustp1(void)
{
	modifyTarget(&tpSteps[advModedata.visableStep].TargetTemperature, 1.0);
	reconstruct_tprog();
}

static void ustp10(void)
{
	modifyTarget(&tpSteps[advModedata.visableStep].TargetTemperature, 10.0);
	reconstruct_tprog();
}

static void ustm1(void)
{
	modifyTarget(&tpSteps[advModedata.visableStep].TargetTemperature, -1.0);
	reconstruct_tprog();
}

static void ustm10(void)
{
	modifyTarget(&tpSteps[advModedata.visableStep].TargetTemperature, -10.0);
	reconstruct_tprog();
}

//--------------------------------------------------------------------------

uint8_t modifyTarget_i(int32_t* val, int8_t diff)
{
	if (*val < 0)
		return 0;
	else
	{
		*val += diff;
		return 1;
	}
}

static void timep1(void)
{
	modifyTarget_i(&tpSteps[advModedata.visableStep].Time, 1);
	reconstruct_tprog();
}

static void timep10(void)
{
	modifyTarget_i(&tpSteps[advModedata.visableStep].Time, 10);
	reconstruct_tprog();
}

static void timem1(void)
{
	struct StepData* currentStep = &tpSteps[advModedata.visableStep];
	modifyTarget_i(&currentStep->Time, -1);
	if (currentStep->Time < 1)
		currentStep->Time = 1;
		
	reconstruct_tprog();
}

static void timem10(void)
{
	struct StepData* currentStep = &tpSteps[advModedata.visableStep];
	modifyTarget_i(&currentStep->Time, -10);
	if (currentStep->Time < 1)
		currentStep->Time = 1;
		
	reconstruct_tprog();
}

//--------------------------------------------------------------------

static void UpdateTermoprogrammExecStatues(enum TermProgStep step)
{
	advModedata.visableStep = step;
}

static void StartCurrentProgramm(void)
{	
	advModedata.visableStep = T_PRG_STEP_IDLE;
	constructTermoprogramm(&prog, dataOutputVars.CurrentTemperature);
	ExecTermoprogramm(&prog);
	
	SaveTermoProgramm(tpSteps);
}

static void ok_btn_callback(void)
{
	switch(advModedata.editStep)
	{
		case ES_IDLE:
			advModedata.editStep = ES_EXEC_DISPLAY;
			btns_clearCallbacks();
			btns[BTN_SEL].onLongPush = toggleLight;
			btns[BTN_OK].onClick = BreakTermoprogramm;
			StartCurrentProgramm();
			break;
		case ES_STEP:
			advModedata.editStep = ES_UST;
			btns[BTN_UP].onClick = ustp1;
			btns[BTN_DOWN].onClick = ustm1;
			btns[BTN_UP].onLongPush = ustp10;
			btns[BTN_DOWN].onLongPush = ustm10;
			btns[BTN_SEL].onLongPush = NULL;
			break;
		case ES_UST:
			if (advModedata.visableStep > T_PRG_STEP_1 && 
				(tpSteps[advModedata.visableStep - 1].TargetTemperature == tpSteps[advModedata.visableStep].TargetTemperature))
			{
				advModedata.editStep = ES_TIME;
				btns[BTN_UP].onClick = timep1;
				btns[BTN_DOWN].onClick = timem1;
				btns[BTN_UP].onLongPush = timep10;
				btns[BTN_DOWN].onLongPush = timem10;
			}
			else
			{
				advModedata.editStep = ES_STEP;
				btns[BTN_UP].onClick = next_prg_step;
				btns[BTN_DOWN].onClick = prev_prg_step;
				btns[BTN_UP].onLongPush = NULL;
				btns[BTN_DOWN].onLongPush = NULL;
				btns[BTN_SEL].onLongPush = removeCurrentStep;
				
				SaveTermoProgramm(tpSteps);
			}
			break;
		case ES_TIME:
			advModedata.editStep = ES_STEP;
			btns[BTN_UP].onClick = next_prg_step;
			btns[BTN_DOWN].onClick = prev_prg_step;
			btns[BTN_UP].onLongPush = NULL;
			btns[BTN_DOWN].onLongPush = NULL;
			btns[BTN_SEL].onLongPush = removeCurrentStep;
			
			SaveTermoProgramm(tpSteps);
			break;
		case ES_EXEC_DISPLAY:
			UI_init();
			break;
	}
}

static void UI_init(void)
{
	advModedata.visableStep = T_PRG_STEP_IDLE;
	advModedata.editStep = ES_IDLE;
	
	LoadTermoprogramm(tpSteps);
	constructTermoprogramm(&prog, dataOutputVars.CurrentTemperature);
	
	btns_clearCallbacks();
	btns[BTN_UP].onClick = next_prg_step;
	btns[BTN_DOWN].onClick = prev_prg_step;
	btns[BTN_SEL].onClick = sel_btn_callback;
	btns[BTN_SEL].onLongPush = toggleLight;
	btns[BTN_OK].onClick = ok_btn_callback;
}

static void Draw1line(struct sDisplayCtrl *display)
{
	Display_printf_P(display, PSTR("%2d "), advModedata.visableStep);
	
	float StartTemperature = dataOutputVars.CurrentTemperature;

	for (enum TermProgStep step = T_PRG_STEP_1; step <= T_PRG_STEP_END; ++step)
	{
		struct StepData s = tpSteps[step];
		
		if (s.stepID != T_PRG_STEP_IDLE)
		{
			if (step == advModedata.visableStep && dataOutputVars.dateTime.second % 2 && advModedata.editStep == ES_STEP)
				Display_putchar(display, Solid);
			else
				Display_putchar(display, s.StepDoCode);
		} else
		{
			if (step == advModedata.visableStep)
			{
				if (dataOutputVars.dateTime.second % 2 && advModedata.editStep == ES_STEP)
					Display_putchar(display, Solid);
				else
					Display_putchar(display, ' ');
			}
			else
				break;
		}
					
		StartTemperature = s.TargetTemperature;
	}
	Display_finishLine(display);
}

static void Draw2line(struct sDisplayCtrl *display)
{
	struct StepData currentStep = tpSteps[advModedata.visableStep];
	if (currentStep.StepDoCode == STEP_HOLD)
	{
		Display_printf_P(display, PSTR("T="));
		if ((advModedata.editStep == ES_UST) && (dataOutputVars.dateTime.second % 2))
			Display_printf_P(display, PSTR("    "));
		else
			Display_printf_P(display, PSTR("%+4.0f"), (double)currentStep.TargetTemperature);
	
		Display_printf_P(display, PSTR("%c t="), degrie_c);
		if ((advModedata.editStep == ES_TIME) && (dataOutputVars.dateTime.second % 2))
			Display_printf_P(display, PSTR("     "));
		else
			Display_printf_P(display, PSTR("%5d"), currentStep.Time);
		Display_putchar(display, 'c');
	} 
	else
	{
		float srcTemp = advModedata.visableStep == T_PRG_STEP_1 ? 
			dataOutputVars.CurrentTemperature : tpSteps[advModedata.visableStep - 1].TargetTemperature;
		Display_printf_P(display, PSTR("%+4.0f%c => "), 
			(double)srcTemp, degrie_c);
		if (dataOutputVars.dateTime.second % 2 && advModedata.editStep == ES_UST)
			Display_printf_P(display, PSTR("    "));
		else
			Display_printf_P(display, PSTR("%+4.0f"), (double)currentStep.TargetTemperature);
		Display_putchar(display, degrie_c);
		Display_finishLine(display);
	}
}

static void Draw1lineR(struct sDisplayCtrl *display)
{
	Display_printf_P(display, PSTR("%2d "), advModedata.visableStep);
	
	for (enum TermProgStep step = T_PRG_STEP_1; step <= T_PRG_STEP_END; ++step)
	{
		if (step < advModedata.visableStep)
			Display_putchar(display, '>');
		else if (step == advModedata.visableStep)
		{
			Display_putchar(display, dataOutputVars.dateTime.second % 2 ? tpSteps[step].StepDoCode : ' ');
			
			++step;
			for (; step <= T_PRG_STEP_END; ++step)
			{
				if (tpSteps[step].stepID != T_PRG_STEP_IDLE)
					Display_putchar(display, getActionCode(tpSteps[step - 1].TargetTemperature, tpSteps[step].TargetTemperature));
				else
					break;
			}
			break;
		}
	}
	
	Display_finishLine(display);
}

static void Draw2lineR(struct sDisplayCtrl *display)
{
	if (tpSteps[advModedata.visableStep].StepDoCode == STEP_HOLD)
	{
		Display_printf_P(display, PSTR("T=%+4.0f%c t=%5dc"), 
			(double)dataOutputVars.CurrentTemperature,
			degrie_c,
			tpSteps[advModedata.visableStep].Time);
	}
	else
	{
		Display_printf_P(display, PSTR("%+4.0f%c => %+4.0f%c"),
			(double)dataOutputVars.CurrentTemperature,
			degrie_c,
			(double)tpSteps[advModedata.visableStep].TargetTemperature,
			degrie_c);

		Display_finishLine(display);
	}
}

static void UI_update(struct sDisplayCtrl *display)
{
	Display_home(display);
	
	Display_SetCursor(display, 0, 0);
	if (advModedata.editStep == ES_IDLE)
	{
		Display_printf_P(display, Gotov);
		Display_finishLine(display);
		
		Display_SetCursor(display, 0, 1);
		Display_printf_P(display, helpStr);
		Display_finishLine(display);
	} else if (advModedata.editStep == ES_EXEC_DISPLAY)
	{
		Draw1lineR(display);
		Display_SetCursor(display, 0, 1);
		Draw2lineR(display);
	} else
	{
		Draw1line(display);
		Display_SetCursor(display, 0, 1);
		Draw2line(display);
	}
}

struct UI_ctr AdvancedUI =
{
	.Draw = UI_update,
	.Init = UI_init,
};

static void sel_btn_callback(void)
{
	switch(advModedata.editStep)
	{
		case ES_IDLE:
			if (AdvancedUI.SwitchModeRequred) // switch mode
				AdvancedUI.SwitchModeRequred();
			break;
		case ES_STEP:
			advModedata.editStep = ES_IDLE;
			btns[BTN_SEL].onLongPush = toggleLight;
			break;
		case ES_UST:
			advModedata.editStep = ES_STEP;
			btns[BTN_UP].onClick = next_prg_step;
			btns[BTN_DOWN].onClick = prev_prg_step;
			btns[BTN_UP].onLongPush = NULL;
			btns[BTN_DOWN].onLongPush = NULL;
			btns[BTN_SEL].onLongPush = removeCurrentStep;
			
			SaveTermoProgramm(tpSteps);
			break;
		case ES_TIME:
			advModedata.editStep = ES_UST;
			btns[BTN_UP].onClick = ustp1;
			btns[BTN_DOWN].onClick = ustm1;
			btns[BTN_UP].onLongPush = ustp10;
			btns[BTN_DOWN].onLongPush = ustm10;
			btns[BTN_SEL].onLongPush = NULL;
			
			SaveTermoProgramm(tpSteps);
			break;
		case ES_EXEC_DISPLAY:
			UI_init();
			break;
	}
}