﻿/*
 * GccUSB.cpp
 *
 * Created: 29.04.2014 8:11:08
 *  Author: max
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <stdio.h>

#include "Config/AppConfig.h"

#include "delay_wraper.h"
#include "SoftI2CMaster.h"
#include "ds1307.h"
#include "MassStorageGenericHID.h"
#include "Clock_INT.h"
#include "Shaduler.h"
#include "Settings.h"
#include "ButtonsProcessor.h"
#include "Lib/24cXXX.h"
#include "Coils.h"
#include "regulator.h"
#include "Display.h"
#include "UI.h"
#include "debug.h"
#include "ButtonsProcessor.h"
	
struct sDisplayCtrl display;	
	
void init_all()
{
#if USE_WDT == 1
	#if WDT_STACK_USAGE == 1
		__asm__ __volatile__ (  \
		"in __tmp_reg__,__SREG__" "\n\t"    \
		"cli" "\n\t"    \
		"wdr" "\n\t"    \
		"sts %0,%1" "\n\t"  \
		"out __SREG__,__tmp_reg__" "\n\t"   \
		"sts %0,%2" "\n\t" \
		: /* no outputs */  \
		: "M" (_SFR_MEM_ADDR(_WD_CONTROL_REG)), \
		"r" (_BV(_WD_CHANGE_BIT) | _BV(WDE)), \
		"r" ((uint8_t) ((WDTO_1S & 0x08 ? _WD_PS3_MASK : 0x00) | _BV(WDIE) | _BV(WDE) | (WDTO_1S & 0x07)) ) \
															/*   interrupt   go to reset*/ \
		: "r0"  \
		);
	#else
		wdt_enable(WDTO_1S);
	#endif
#endif
	SetupHardware(); // LUFA hardware init operations	
	
	i2c_init(); // I2C
	i2c_SetSpeed(I2C_SPEED_FAST);
	Coils_Init(); // coils control
	
	ACSR = 1 << ACD; // disavle analog comparator
				
	ds1307_init(); // Часы
	
#if DISPLAY_ENABLED == 1
	Display_constructor(&display, PCF_ADDR_SETUP, 16, 2, LCD_BACKLIGHT);
	Display_init(&display, LCD_5x8DOTS);
#endif
	
	set_sleep_mode(SLEEP_MODE_IDLE); // режим сна
	ACSR &= ~(1 << ACD); // disable analog comparator
	
	Shaduler_enable(1); //планировщик
	
#if BUTTONS_ENABLE == 1
	ButtonsInit();
#endif
	
#if USB_ENABLED == 1
	USB_Init();
#endif

	GlobalInterruptEnable(); // sei()
	
	regulator_init();
	
#if DISPLAY_ENABLED == 1
	UI_setDisplay(&display);
#endif	
}

void setDateTime()
{
	static struct ds1307DateTime initdate = 
	{
		.second = 0,
		.minute = 0,
		.hour = 0,
		.DoW = 0,
		.day = 1,
		.month = 1,
		.year = 0		
	};
	ds1307_setdate(&initdate);
}

int main(void)
{	  
	init_all();
	
	//setDateTime(); // Установить в часы дату (тест)

	while(1)
    {			
#if USE_WDT == 1
		wdt_reset();
#endif		

		/*
		if (tickPassed)
		{
			Tick();
			tickPassed = 0;
		}
		*/
		
		//Display_SetCursor(&display, 0, 1);
		//Display_printf(&display, "test");
				
		ProcessShaduled();
		
#if USB_ENABLED == 1		
		// process USB events
		LUFA_main();
#else
		sleep_cpu();
#endif
    }
}

// default interrupt routine
ISR(BADISR_vect)
{
}

#if WDT_STACK_USAGE == 1
ISR(WDT_vect)
{
	// все плохо! вылет по собаке, поэтому сохраним стек
	DEBUG_SAVE_SP(1);	
}
#endif