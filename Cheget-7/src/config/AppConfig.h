/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *  \brief Application Configuration Header File
 *
 *  This is a header file which is be used to configure some of
 *  the application's compile time options, as an alternative to
 *  specifying the compile time constants supplied through a
 *  makefile or build system.
 *
 *  For information on what each token does, refer to the
 *  \ref Sec_Options section of the application documentation.
 */

#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_

	#define FIRST_RUN							1
	#define USB_ENABLED							1
	#define MASS_STORAGE_ENABLED				1
	#define USB_CONTROLS_LEDS					0
	#define USE_WDT								1
	#define DISPLAY_ENABLED						1
	#define MESURMENT_ENABLE					1
	#define BUTTONS_ENABLE						1
	#define SLEEP_PERIOD_US						3
	#define USE_FAST_SHADULER					1
	
	#define PCF_ADDR_SETUP						(0b111 << 1)
	
	#if WDT_STACK_USAGE == 1
		#undef USE_WDT
		#define USE_WDT	1
	#endif
	
	// HID
	#include "ds1307.h"

	struct my_HID_Report
	{
		struct ds1307DateTime dateTime;
		uint8_t dumy1; // align float requred
		float CurrentTemperature;
		float TargetTemperature;
		uint8_t isCooling;
		uint8_t startStop;
	};
	
	#define GENERIC_REPORT_SIZE				sizeof(struct my_HID_Report)

	// mass storage
	#define TOTAL_LUNS						1

	#define DISK_READ_ONLY					0

#endif
