#ifndef _24CXXX_H_
#define _24CXXX_H_

#define FALSE 0
#define TRUE 1

#define EEPROM_PIN_CONFIG		0b00
#define EEPROM_ADDRESS(RW)		(0b10100000 | (EEPROM_PIN_CONFIG << 1) | RW)

#define EEPROM_READ				1
#define EEPROM_WRITE			0

#include "SoftI2CMaster.h"

char EEBegin(uint16_t addr);

void EEWriteByte(uint16_t,uint8_t);
uint8_t EEReadByte(uint16_t address);

void EEWriteBytes(uint16_t dest_addr, uint8_t *__src, uint16_t size);
void EEReadBytes(uint8_t* buf, uint16_t address, uint16_t size);

#endif
