/*
 * Shaduler.c
 *
 * Created: 27.05.2014 14:06:18
 *  Author: tolyan
 */ 

#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <string.h>
#include <math.h>
#include <avr/wdt.h>
#include <util/delay.h>

#include "Config/AppConfig.h"
#include "ButtonsProcessor.h"
#include "debug.h"
#include "dataOutputVars.h"
#include "UI.h"
#include "Clock_INT.h"
#include "Termoprogramm.h"

#include "MassStorageGenericHID.h"

#include "Shaduler.h"

#define FAST_SHADULER_TICK_VAL		0.1

const struct Task tasks[] EEMEM = 
{
	{
		updateTime, 1
	},
	{
		TermoprogrammTick_1s, 0.5	
	},
#if BUTTONS_ENABLE == 1
	{
		RegularButtonsProcess, 0.1
	},
#endif	
#if MESURMENT_ENABLE == 1
	{
		updateCurrent, 1
	},/*
	{
		updateTarget, 10
	},*/
#endif
#if DISPLAY_ENABLED == 1
	{
		updateDisplay, 1
	},
#endif
	{
		NULL, 0
	}
};

static struct Task tasks_work[sizeof(tasks)/sizeof(struct Task)];

static uint8_t taskQueue_wp = 0;
static uint8_t taskQueue_rp = 0;
static uint8_t taskQueue_used = 0;
static taskRoutine taskQueue[16];

static void reloadTask(uint8_t id)
{
	eeprom_read_block(&tasks_work[id], &tasks[id], sizeof(struct Task));
}

void initShadulerTimer()
{
	TCCR0B |= (1 << CS02) | (1 << CS00); //F = clk_io/1024 (16000000/1024)
	TIMSK0 |= 1 << TOIE0; // overflow interrupt
}

void Shaduler_enable(uint8_t enable)
{
	if (enable)
	{
		for (uint8_t i = 0; i < sizeof(tasks)/sizeof(struct Task); ++i)
			reloadTask(i);
	}
	else
		memset(tasks_work, 0, sizeof(tasks_work));
#if USE_FAST_SHADULER == 1
	initShadulerTimer();
#else
	Clock_INT_Enable(enable); //������� ����������
#endif
}


void Tick()
{
#if USE_WDT == 1
	wdt_reset();
#endif
	const float tick = 
#if USE_FAST_SHADULER == 1
	FAST_SHADULER_TICK_VAL;
#else
	DOUBLE_REFRASH_RATE ? 0.5 : 1;
#endif
	for (uint8_t TaskID = 0; TaskID < sizeof(tasks)/sizeof(struct Task); ++TaskID)
	{
		taskRoutine Routine = tasks_work[TaskID].Routine;
		if (Routine)
		if ((tasks_work[TaskID].period -= tick) <= 0)
		{
			shadule(Routine);
			reloadTask(TaskID);
		}
	}
}

void ProcessShaduled()
{
	while (taskQueue_used)
	{
#if USE_WDT == 1
		wdt_reset();
#endif
		taskRoutine shaduledRoutine = taskQueue[taskQueue_rp++];
		if (taskQueue_rp == sizeof(taskQueue) / sizeof(taskRoutine))
			taskQueue_rp = 0;
		--taskQueue_used;
		shaduledRoutine();
	}
}

uint8_t shadule(taskRoutine routine)
{
	uint8_t res;

	if (taskQueue_used == sizeof(taskQueue) / sizeof(taskRoutine))
		res = 0;
	else
	{
		taskQueue[taskQueue_wp++] = routine;
		if (taskQueue_wp == sizeof(taskQueue) / sizeof(taskRoutine))
			taskQueue_wp = 0;

		++taskQueue_used;

		res = 1;
	}

	return res;
}

void shaduler_ProcessTimeCrytical()
{
#if USB_ENABLED == 1	
	// process USB events
	LUFA_main();
#endif
}

void Shaduler_wait(uint32_t delayus)
{
	while (delayus > SLEEP_PERIOD_US)
	{
#if USE_WDT == 1
		wdt_reset();
#endif
		shaduler_ProcessTimeCrytical();
		_delay_us(SLEEP_PERIOD_US);
		delayus -= SLEEP_PERIOD_US;
	}
}

ISR(TIMER0_OVF_vect)
{
	static uint16_t devider = 0;
	if (devider % 6 == 0)
	{
		Tick();
	}
	++devider;
}