/*
 * ButtonsProcessor.c
 *
 * Created: 30.05.2014 14:26:00
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <stddef.h>
#include <avr/interrupt.h>
#include <string.h>

#include "Shaduler.h"
#include "dataOutputVars.h"
#include "UI.h"

#include "ButtonsProcessor.h"

#define BTN_UP_PORT		PORTD
#define BTN_UP_DIR		DDRD
#define BTN_UP_IN		PIND
#define BTN_UP_PIN		1

#define BTN_DOWN_PORT	PORTE
#define BTN_DOWN_DIR	DDRE
#define BTN_DOWN_IN		PINE
#define BTN_DOWN_PIN	7

#define BTN_SEL_PORT	PORTE
#define BTN_SEL_DIR		DDRE
#define BTN_SEL_IN		PINE
#define BTN_SEL_PIN		5

#define BTN_OK_PORT		PORTE
#define BTN_OK_DIR		DDRE
#define BTN_OK_IN		PINE
#define BTN_OK_PIN		4

#define IS_INVERTED		1

struct sButtonCallbacks btns[4];

void btns_clearCallbacks()
{
	memset(btns, 0, sizeof(btns));
}

void ButtonsInit()
{
	BTN_UP_DIR &= ~(1 << BTN_UP_PIN);
	BTN_DOWN_DIR &= ~(1 << BTN_DOWN_PIN);
	BTN_SEL_DIR &= ~(1 << BTN_SEL_PIN);
	BTN_OK_DIR &= ~(1 << BTN_OK_PIN);

	// interrupts (any adge)
	EICRA &= ~(1 << ISC11);
	EICRA |= (1 << ISC10);
	EICRB &= ~((1 << ISC41) | (1 << ISC51) | (1 << ISC71));
	EICRB |= ((1 << ISC40) | (1 << ISC50) | (1 << ISC70));
	
	// unmask interrupts
	EIMSK |= ((1 << 1) | (1 << 4) | (1 << 5) | (1 << 7));
}

static uint8_t getBthState(enum enBtnID btn)
{
	uint8_t res;
	
	switch(btn)
	{
		case BTN_UP : 
			res = BTN_UP_IN & (1 << BTN_UP_PIN);
			break;
		case BTN_DOWN : 
			res = BTN_DOWN_IN & (1 << BTN_DOWN_PIN);
			break;
		case BTN_SEL : 
			res = BTN_SEL_IN & (1 << BTN_SEL_PIN);
			break;
		case BTN_OK : 
			res = BTN_OK_IN & (1 << BTN_OK_PIN);
			break;
		default : return 0;
	}	
	
#if IS_INVERTED == 1	
	return res ? 0 : 1;
#else
	return res ? 1 : 0;
#endif
}

void RegularButtonsProcess()
{
	for (uint8_t i = 0; i < sizeof(btns) / sizeof(struct sButtonCallbacks) ; ++i)
	{
		uint8_t* p = &btns[i].lastState;
		*p = (*p << 1) | getBthState(i);
		if ((*p & 0b1111111) == 0b1111111)
		{
			*p &= ~1;
			btns[i].LongPressDetected = 1;
			if (btns[i].onLongPush)
			{
				shadule(btns[i].onLongPush);
				shadule(updateDisplay);
			}
		}
	}
}

static void btnEvent(enum enBtnID b)
{
	uint8_t ls = btns[b].lastState;
	switch (ls & 0b11)
	{
		case 0b11 : 
			//release
			btns[b].lastState = (ls << 1);
			if (btns[b].onRelease)
			{
				shadule(btns[b].onRelease);
				shadule(updateDisplay);
			}
			if (!btns[b].LongPressDetected && btns[b].onClick) 
			{
				shadule(btns[b].onClick);
				shadule(updateDisplay);
			}
			break;
		case 0b00 :
			// press
			btns[b].LongPressDetected = 0;
			btns[b].lastState = (ls << 1) | 1;
			if (btns[b].onPress)
			{
				shadule(btns[b].onPress);
				shadule(updateDisplay);
			}
			break;
	}
}

ISR(INT7_vect)
{ // DOWN
	btnEvent(BTN_DOWN);
}

ISR(INT1_vect)
{ // UP
	btnEvent(BTN_UP);
}

ISR(INT4_vect)
{ // OK
	btnEvent(BTN_OK);
}

ISR(INT5_vect)
{ // SEL
	btnEvent(BTN_SEL);
}