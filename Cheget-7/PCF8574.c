/*
 * PCF8574.c
 *
 * Created: 13.07.2015 16:32:39
 *  Author: tolyan
 */ 

#include <stdint.h>

#include "SoftI2CMaster.h"

#include "PCF8574.h"

static uint8_t i2cAddr;
static uint8_t AllwaysSetMask = 0;
static uint8_t AllwaysCleanMask = 0;

void Extender_Init(uint8_t addr)
{
	i2cAddr = addr;
	Extender_Write(0);
}

static uint8_t _begin()
{
	i2c_start(i2cAddr | I2C_WRITE);
}

static void _end()
{
	i2c_stop();
}

void Extender_Write(uint8_t value)
{
	_begin();
	value |= AllwaysSetMask;
	value &= ~AllwaysCleanMask;
	i2c_write(value);
	_end();
}

void Extender_setAllwaysSet(uint8_t mask)
{
	AllwaysSetMask = mask;
}

void Extender_setAllwaysClear(uint8_t mask)
{
	AllwaysCleanMask = mask;
}