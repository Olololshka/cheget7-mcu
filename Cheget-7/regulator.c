/*
 * regulator.c
 *
 * Created: 04.12.2014 13:22:46
 *  Author: tolyan
 */ 

#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <math.h>
#include <string.h>

#include "chanize_modbus_master.h"
#include "lib/uart.h"
#include "Shaduler.h"
#include "dataOutputVars.h"

#include "regulator.h"

#define CUR_TEMP_ADDR			0x0000
#define TARGET_TEMP_ADDR		0x0010
#define HYSTERESIS_ADDR			0x0140
#define ALARM_VAL_ADDR			0x0040

#define RS485_TX_PIN			4
#define RS485_TX_PORT			PORTD

#define UART_SPEED				settings_getu16(offsetof(struct sSettings, uart_speed))
#define REGULATOR_ADDR			settings_getu8(offsetof(struct sSettings, regulator_addr))
#define RX_TIMEOUT_MS			settings_getu16(offsetof(struct sSettings, rx_timeout))
#define HYSTERESIS				settings_getfloat(offsetof(struct sSettings, hysteresis))

static void tx_enabe_callback()
{
	RS485_TX_PORT |= 1 << RS485_TX_PIN;
	UCSR1B |= (1 << TXCIE1);
	
	// disable ressiver
	UCSR1B &= ~(1 << RXEN1);
}

static void tx_disable_callback()
{
	RS485_TX_PORT &= ~(1 << RS485_TX_PIN);
	
	//enable ressiver
	UCSR1B |= (1 << RXEN1);
}

static void uart_flush()
{
	while (uart_is_data_avalable())
		uart_getc();
}

ISR(USART1_TX_vect)
{
	tx_disable_callback();
	UCSR1B &= ~(1 << TXCIE1);
}

void regulator_init()
{
	uart_init(UART_BAUD_SELECT(UART_SPEED, F_CPU));
	uart_register_TX_callbacks(tx_enabe_callback, NULL);
	DDRD |= (1 << RS485_TX_PIN); // enable RE/DE
	tx_disable_callback(); // RESSIVE
	uart_flush();
	
	Regulator_setIdle();
}

static void write_uart(uint8_t* buf, uint16_t len)
{
	int i;
	for (i = 0; i < len; ++i)
	{
		shaduler_ProcessTimeCrytical();
		uart_putc(buf[i]);
	}
}

static int getAnsver(int* dest)
{
	uint16_t timeout_ms = RX_TIMEOUT_MS;
	uint16_t len = 0;
	int res;
	uint8_t mbframe[32];
	
	while(timeout_ms--)
	{
		Shaduler_wait(1000);
		while (uart_is_data_avalable())
			mbframe[len++] = uart_getc();
		if (len) {
			if ((res = rtu_data_anlys(dest, mbframe, 0, len)) == 0)
				break;
		}
	}
	
	uart_flush();
	
	return res;
}

static int write_controller(uint16_t addr, int16_t val)
{
	uint8_t buf[32];
	int len = rtu_set_hldreg(REGULATOR_ADDR, buf, addr, val);
	write_uart(buf, len);
	len = getAnsver((int*)buf);
	
	return len;
}

static int read_controller(int16_t *dest, uint16_t addr)
{
	uint8_t buf[32];
	int len = rtu_read_hldreg(REGULATOR_ADDR, buf, addr, 1);
	write_uart(buf, len);
	len = getAnsver((int*)dest);
	
	return len;
}

float getCurrentTemperature()
{
	int16_t val16;
	int res;
	if (!(res = read_controller(&val16, CUR_TEMP_ADDR)))
		return val16 / 10.0;
	else
		return NAN;
}

float getTargetTemperature()
{
	int16_t val16;
	int res;
	if (!(res = read_controller(&val16, TARGET_TEMP_ADDR)))
		return val16 / 10.0;
	else
		return NAN;
}

uint8_t setTemperatureHisteresis(float histeresis)
{
	return write_controller(HYSTERESIS_ADDR, (int16_t)(histeresis * 10.0));
}

uint8_t setTargetTemperature(float target)
{/*
	uint8_t minus = 0;
	if (target < 0)
	{
		minus = 1;
		target = -target;
	}
	int16_t v = lround(target * 10.0);
	return write_controller(TARGET_TEMP_ADDR, minus ? -v : v);
	*/
	return write_controller(TARGET_TEMP_ADDR, (int16_t)(target * 10.0));
}

uint8_t setTemperatureAlarm(float alarm)
{
	return write_controller(ALARM_VAL_ADDR, (int16_t)(alarm * 10.0));
}

uint8_t Regulator_setIdle()
{
	dataOutputVars.TargetTemperature = IDLE_TEMPERATURE;
	return setTargetTemperature(IDLE_TEMPERATURE);
}
