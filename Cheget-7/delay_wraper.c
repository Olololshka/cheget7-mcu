/*
 * delay_wraper.c
 *
 * Created: 14.05.2014 8:48:57
 *  Author: tolyan
 */ 

#define __DELAY_BACKWARD_COMPATIBLE__
#include <util/delay.h>
#include "delay_wraper.h"

#ifdef DELAY_WRAPER

void delay_ms(double __ms)
{
	_delay_ms(__ms);
}

void delay_us(double __us)
{
	_delay_us(__us);
}

#endif
