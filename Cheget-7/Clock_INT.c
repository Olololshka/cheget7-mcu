/*
 * INT0.c
 *
 * Created: 15.05.2014 10:51:23
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include "Shaduler.h"

#include "Clock_INT.h"

#define INT6_PIN	6
#define INT6_DDR	DDRE

uint8_t tickPassed = 0;

void Clock_INT_Enable(char enable)
{
	INT6_DDR &= ~(1 << INT6_PIN); //input
	if (enable)
	{
#if DOUBLE_REFRASH_RATE == 1
		EICRB |= 1 << ISC60;
		EICRB &= ~(1 << ISC61);
#else
		//falling age
		EICRB |= 1 << ISC61;
		EICRB &= ~(1 << ISC60);
#endif	
		EIMSK |= 1 << INT6;
	}
	else
		EIMSK &= ~(1 << INT6);
}

// interrupt itself
ISR(INT6_vect)
{
	//tickPassed = 1;
	Tick();
}